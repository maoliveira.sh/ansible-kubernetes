ansible

- argumentos
	-b => sudo 
	-k => senha inicial (login, usar quando não usar .pem)
	-K => senha sudo (senha sudo, usar quando criar par de chaves manualmente pois as chaves manuais não dão permissão de sudo sem pedir senha)
  --ssh-common-args='-o StrictHostKeyChecking=no'
  --key-file="./ansible.pem"

- automações
	alias agente='ssh-agent bash && ssh-add ~/.ssh/ansible.pem'
  alias agente='ssh-agent bash && ssh-add ~/work/ansible/initial-example/ansible.pem'

- exemplos

	ansible -i hosts all -m shell -a "uptime" -b -K

	ansible -i hosts all -m apt -a "update_cache=yes name=cmatrix state=present" -b -kK

	ansible -i hosts all -m ping 

	ansible -i hosts all -b -K -m copy -a "src=./ok dest=/temp"

	ansible -i hosts all -m git -a "repo=https://github.com/matheusolivv/HelloWorld.git dest=/tmp/ok"

	ansible -i hosts all -m setup

	ansible -i hosts us01 -m setup -a "filter=ansible_default_ipv4"


# ansible -i hosts playbook.yml
---
- hosts: servers
  become: yes
  remote_user: matheus
  tasks:
  - name: Instalando o Nginx
    apt:
      name: nginx
      state: latest
      update_cache: yes

  - name: Habilitanto o nginx no boot
    service:
      name: nginx
      enabled: yes

  - name: Iniciando o nginx
    service:
      name: nginx
      state: started

  - name: copiando index.html para os servidores
    copy:
      src: index.html
      dest: /var/www/html/index.html
    notify: Restartando o Nginx #chama o danhler pois os handlers nao startam automaticamente

  handlers:
  - name: Restartando o Nginx
    service:
      name: nginx
      state: restarted

---------
Conteúdo k8s

# ansible-galaxy init roles

